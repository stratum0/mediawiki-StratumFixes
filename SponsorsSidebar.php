<?php
#
# Add a sponsor link to the sidebar for the "sponsorlink" item
#
$wgHooks['SkinTemplateOutputPageBeforeExec'][] = 'stratumSponsorsHook';
function stratumSponsorsHook($skin, $tpl) {
	$sponsors = array(
		#array(
		#	"name" => "Stratum 0",
		#	"image" => "120px-Sanduhr-wiki-logo.svg.png"
		#),
		array(
			"name" => "TRIOLOGY – Agility for your business",
			"image" => "TRIO_Logo-o-Slogan_web.png"
		),
		#array(
		#	"name" => "GNS Systems",
		#	"image" => "GNS-Systems-Logo-1c-HKS41_neu2014-120x120.png"
		#),
		#array(
		#	"name" => "BEL NET",
		#	"image" => "RZ_BelnetLogo_120x120.jpg"
		#),
		#array(
		#	"name" => "LINET Services",
		#	"image" => "Linet_logo_cmyk.png"
		#),
	);

	if(isset($tpl->data['sidebar']['sponsorlink'])) {
		$i = rand(0, count($sponsors) - 1);
		$v = $sponsors[$i];
		$tpl->data['sidebar']['sponsorlink'] =
			sprintf("<p style='text-align:center'><img alt='%s'".
			" title='%s' src='/mediawiki/images/sponsors/%s' /></p>", $v["name"],
			$v["name"], $v["image"]);
	}
  return true;
}
?>
