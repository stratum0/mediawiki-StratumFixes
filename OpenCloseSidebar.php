<?php
#
# Add the Open/Close status to the sidebar for the 'openclosestatus' item
#
$wgHooks['SkinTemplateOutputPageBeforeExec'][] = 'stratumOpenCloseSidebarHook';
function stratumOpenCloseSidebarHook($skin, $tpl) {
	if(isset($tpl->data['sidebar']['openclosestatus']))
		$tpl->data['sidebar']['openclosestatus'] = '<ul><li><a title="'.
		'&Ouml;ffnungsstatus" href="/wiki/Open/Close-Monitor">'.
		'<img style="border:none; height:2em;" src="https://stratum0.org/status/'.
		'status.png" /></a></li></ul>';
  return true;
}
?>
